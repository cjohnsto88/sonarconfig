package sonar.example;

import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class AdderTest {

	private Adder adder;

	@Before
	public void setUp() throws Exception {
		adder = new Adder();
	}

	@Test
	public void testAdd() throws Exception {
		int result = adder.add(2, 3);

		assertThat(result, equalTo(5));
	}
}